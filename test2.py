import whisper
import os
import wave
import math
from pydub import AudioSegment

# files                                                                       
src = "data/test_audio.mp3"
dst = "data/test.wav"

# convert  mp3 to wav                                                             
audSeg = AudioSegment.from_mp3("data/test_audio.mp3")
audSeg.export(dst, format="wav")

#chose the model 
model = whisper.load_model('base')
#model = whisper.load_model('medium')
#model = whisper.load_model('large')

newAudio = AudioSegment.from_wav("data/test.wav")

#nombre de partions
nb_partion = 15

start = 0 
pas= len(newAudio)/nb_partion
listAudio = []

for i in range(nb_partion):
    listAudio.append(newAudio[start:start+pas])
    start= pas*i +1
j=1
for i in listAudio:
    print("--------------Partion: ", j," -------------------")
    i.export(dst, format="wav")
    out = model.transcribe(dst)
    print(out['text'])
    j+=1
