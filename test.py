import torch
import torchaudio 
import os 
import moviepy.editor as mv
import whisper 
import streamlit as st
import numpy as np
import wave 
import matplotlib.pyplot as plt

#pip install pyannote.core
#pip install torchaudio

"""st.title("E-learning Plateform")
video_file = open("test_video.mp4",'rb')
video_bytes = video_file.read()
st.video(video_bytes)"""

# one 4-dimensional feature vector extracted every 100ms from a 200ms window
frame = SlidingWindow(start = 0.0, step = 0.001, duration = 0.200)
data = np.random.randn(100, 4)
features = SlidingWindowFeature(data, frame)
f = 'test_audio.mp3'
#model = whisper.load_model("base")

metadata = torchaudio.info(f)
for segment in frame :
    #print(type(segment))
    signal, sample_rate = torchaudio.load(f, frame_offset = int(segment.start*metadata.sample_rate), num_frames=int((segment.end-segment.start)*metadata.sample_rate))#,
    
